from collections import namedtuple
from re import search
from sys import argv,stdin
from os import path
from requests import get

import json
import argparse

USER_ALIASES = {'n':str(86420832),'y':str(108416569)}

APP_ID 			= str(4904533)
REDIRECT_URI 	= "https://oauth.vk.com/blank.html"
PERMISSIONS  	= "messages,friends,offline" 
URL = namedtuple('URL','url parameters')

def get_user_id(nickname):
	if nickname in USER_ALIASES:
		return USER_ALIASES[nickname]
	else:
		FIND_USER = URL(
			url 		= 'https://api.vk.com/method/friends.search?',
			parameters 	= 'q='+nickname+\
						  '&count='+str(5)+\
						  '&access_token='+API_KEY
		)
		r = get(FIND_USER.url+FIND_USER.parameters)
		data = json.loads(r.text)['response']
		print "Found %d users" % data[0]
		if data[0] == 0:
			exit(1)
		if data[0] == 1:
			return data[1]['uid']
		print "Choose one of them:"
		friends = data[1:]
		for i in xrange(0,len(friends)):
			first_name = friends[i]['first_name']
			last_name = friends[i]['last_name'] 
			print "%d. %s %s" % (i,first_name,last_name)
		ind = int(raw_input('> '))
		return str(friends[ind]['uid'])

def get_api_key():
	api_key_path = path.dirname(path.abspath(__file__)) + '/api_key'
	try:
		with open(api_key_path,'r') as f:
			API_KEY = f.read()
	except IOError:
		OAUTH = URL(
			url 	   = "https://oauth.vk.com/authorize?",
			parameters = "client_id="+APP_ID+\
						 "&scope="+PERMISSIONS+\
						 "&redirect_uri="+REDIRECT_URI+\
						 "&display=page&v=5.30&response_type=token"
		)
		try:
			from selenium import webdriver
			from selenium.webdriver.common.by import By
			from selenium.webdriver.support.ui import WebDriverWait
			from selenium.webdriver.support import expected_conditions as EC
	
			driver = webdriver.Firefox()
			driver.get(OAUTH.url+OAUTH.parameters)
			try:
				WebDriverWait(driver, 60).until(
		        	EC.title_contains("Blank")
		    	)
			except TimeoutException:
				driver.quit()
			API_KEY = search('access_token=([^&]+)',driver.current_url).group(1)
			driver.quit()
		except ImportError:
			API_KEY = raw_input("You have no selenium installed so please visit %s and paste obtained API key here:\n> " 
					  % (OAUTH.url+OAUTH.parameters)).strip()
		with open(api_key_path,'w') as f:
			f.write(API_KEY)
	return API_KEY
	
def build_parser():
	arg_parser = argparse.ArgumentParser(description='Allows to send private messages vkontakte')
	arg_parser.add_argument('addressee', 
							help='''define target of your message. It can be hardcoded alias,
									first name, last name, nikname or user id''')
	arg_parser.add_argument('-f', '--file',
							dest='file', metavar='file_name',
							help='File that would be attached as a document')
	arg_parser.add_argument('-i', '--image',
							dest='image', metavar='img_name',
							help='Image that would be attached as a photo')
	arg_parser.add_argument('message',
							nargs='*', 
							help='Message to send (can be in several words)')
	return arg_parser

def send_message(addr, message, API_KEY):
	SEND_MESSAGE = URL(
		url 		= 'https://api.vk.com/method/messages.send?',
		parameters 	= 'user_id='+addr+\
					  '&message='+message+\
					  '&access_token='+API_KEY
	)
	
	r = get(SEND_MESSAGE.url+SEND_MESSAGE.parameters)
	return r.status_code

if __name__ == "__main__":
	args = build_parser().parse_args()
	API_KEY = get_api_key()
	# select source of the message
	if args.message:
		if len(args.message) > 1:
			source = [' '.join(args.message)]
		else:
			source = args.message
	else:
		source = stdin
	# find addressee's id
	try:
		addr = get_user_id(args.addressee)
	except:
		print "\nError ocured while processing addressee"
		exit(1)
	#send messages
	for line in source:
		send_message(addr, line, API_KEY)
